python_env="$PWD/venv"
if [ ! -d "$python_env" ]; then
    python -m venv "$python_env"
    chmod +x "$python_env/bin/activate"
    echo "New python virtual environment: $python_env"
fi

echo "Install pip requirements"
$python_env/bin/pip install -r requirements.txt

build_directory="$PWD/build"
if [ ! -d  "$build_directory" ]; then
    mkdir "$build_directory"
    echo "Build directory created: $build_directory"
fi

conan_cmd=$python_env/bin/conan
export CONAN_HOME=$build_directory/cache
echo "Conan cache: $build_directory"

conan_profile="$build_directory/conan_profile"
if [ ! -f "$conan_profile" ]; then
    echo "Create conan profile: $conan_profile"
    $conan_cmd profile detect --name "$conan_profile"
fi

build_directory_debug="$build_directory/debug"
if [ ! -d "$build_directory_debug" ]; then
    mkdir "$build_directory_debug"
    $conan_cmd "Debug build directory created: $build_directory_debug"
fi

$conan_cmd install . -pr:h="$conan_profile" -pr:b="$conan_profile" -of="$build_directory_debug" --build=missing -s build_type="Debug"

build_directory_release="$build_directory/release"
if [ ! -d "$build_directory_release" ]; then
    mkdir "$build_directory_release"
    echo "Release build directory created: $build_directory_release"
fi

$conan_cmd install . -pr:h="$conan_profile" -pr:b="$conan_profile" -of="$build_directory_release" --build=missing -s build_type="Release"
