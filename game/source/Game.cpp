#include "Engine/core/Application.hpp"


#ifdef STANDALONE_BUILD
namespace ce::application {

    bool setup(Engine* engine) {
        LOG_INFO("Game setup")

        return true;
    }

    void update(Engine* engine, float dt) {

    }

    void render(Engine* engine) {

    }

    void shutdown(Engine* engine) {
        LOG_INFO("Game shutdown")
    }

} // ce::application
#endif