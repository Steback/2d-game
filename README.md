# Combat Game
This is  in progress of a remake, the roadmap will be:
* [ ] Basic plane movement
* [ ] Enemies spawn
* [ ] Collisions
* [ ] Combat system
* [ ] Map load

All this game the main goal is to use a custom game engine and support basic modding and also will be a Map Editor in other repo.

## Dependencies
All are installed using the [Conan](https://conan.io/) package manager.

| Name                                       | Version | Licence                                                      |
|--------------------------------------------|---------|--------------------------------------------------------------|
| [fmt](https://github.com/fmtlib/fmt)       | 10.2.1  | [License](https://github.com/fmtlib/fmt/blob/master/LICENSE) |
| [spdlog](https://github.com/gabime/spdlog) | 1.13.0  | [MIT](https://github.com/gabime/spdlog/blob/v1.x/LICENSE)    |
| [GLFW](https://github.com/glfw/glfw)       | 3.3.8   | [zLib](https://github.com/glfw/glfw/blob/master/LICENSE.md)  |

## Build
### Requirements
* [CMake](https://cmake.org/) >= v3.27.
* [Conan](https://conan.io/) >= v2.0.5.
* [Python](https://www.python.org/) >= v3.11.3.

### Linux
* [GCC](https://gcc.gnu.org/) >= v13.2.1.
```
./setup.sh
cmake -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=build/release/conan_toolchain.cmake . -B build/release
cmake --build "build/release"
```

### Windows
* [Visual Studio](https://visualstudio.microsoft.com/) >= 2022
```
.\setup.bat
cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 17 2022" -DCMAKE_TOOLCHAIN_FILE="build/release/conan_toolchain.cmake" . -B build/release
cmake --build "build/release" --config Release
```

## Legacy 
This is old version, keep this until the remake is done, the old code is move to `legacy` branch

![Screenshot-1](docs/images/Screenshot_1.png)
![Screenshot-2](docs/images/Screenshot_2.png)
![Screenshot-3](docs/images/Screenshot_3.png)
![Screenshot-4](docs/images/Screenshot_4.png)
![Screenshot-5](docs/images/Screenshot_5.png)
![Screenshot-6](docs/images/Screenshot_6.png)
