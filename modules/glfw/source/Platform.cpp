#include "Engine/platform/Platform.hpp"

#include <GLFW/glfw3.h>

#include "Engine/core/Logger.hpp"


namespace ce::platform {

    bool setup(Platform* platform, const char* name, int width, int height) {
        if (!glfwInit()) {
            LOG_FATAL("Failed to initialize GLFW")
            return false;
        }

        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        platform->internal = glfwCreateWindow(width, height, name, nullptr, nullptr);
        if (!platform->internal) {
            LOG_FATAL("Failed to created GLFW window")
            return false;
        }

#ifdef CE_PLATFORM_LINUX
        LOG_INFO("Platform: Linux")
#elif defined(CE_PLATFORM_WINDOWS)
        LOG_INFO("Platform: Windows")
#endif

        return true;
    }

    void shutdown(Platform* platform) {
        GLFWwindow* window = (GLFWwindow *)platform->internal;
        if (window != nullptr) {
            glfwDestroyWindow(window);

            glfwTerminate();

            platform->internal = nullptr;

            LOG_INFO("Platform shutdown")
        }
    }

    bool processEvents(Platform* platform) {
        GLFWwindow* window = (GLFWwindow*)platform->internal;

        glfwPollEvents();

        return !glfwWindowShouldClose(window);
    }

    void* allocate(uint64_t size, bool aligned) {
        return malloc(size);
    }

    void free(void* block, bool aligned) {
        ::free(block);
    }

    void* zeroMemory(void* block, uint64_t size) {
        return memset(block, 0, size);
    }

    void* copyMemory(void* dst, const void* src, uint64_t size) {
        return memcpy(dst, src, size);
    }

    void* setMemory(void* dst, int32_t value, uint64_t size) {
        return memset(dst, value, size);
    }

    double getTime() {
        return glfwGetTime();
    }

} // ce::platform