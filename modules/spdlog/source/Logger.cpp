#include "Engine/core/Logger.hpp"

#include <filesystem>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>


static std::shared_ptr<spdlog::logger> logger = nullptr;


namespace ce::log {

    bool setup(const std::string& filename) {
        if (logger != nullptr) {
            return true;
        }

        const auto path = std::filesystem::current_path() / filename;
        if (exists(path)) {
            std::filesystem::remove(path);
        }

        std::vector<spdlog::sink_ptr> sinks;
        sinks.push_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
        sinks.push_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>(path.string()));

        logger = std::make_shared<spdlog::logger>(filename, sinks.begin(), sinks.end());

        if (logger) {
            logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%^%l%$] %v");

#ifdef LOG_TRACE_ENABLED
            logger->set_level(spdlog::level::trace);
#endif
        }

        return logger != nullptr;
    }

    void shutdown() {
        logger.reset();
    }

    void log(Level level, const char* message) {
        switch (level) {
            case Fatal:
                logger->critical(message);
                break;
            case Error:
                logger->error(message);
                break;
            case Warn:
                logger->warn(message);
                break;
            case Info:
                logger->info(message);
                break;
            case Debug:
                logger->debug(message);
                break;
            case Trace:
                logger->trace(message);
                break;
        }
    }

}  // ce::log
