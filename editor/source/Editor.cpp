#include "Engine/core/Application.hpp"


namespace ce::application {

    bool setup(Engine* engine) {
        LOG_INFO("Editor setup")

        return true;
    }

    void update(Engine* engine, float dt) {

    }

    void render(Engine* engine) {

    }

    void shutdown(Engine* engine) {
        LOG_INFO("Editor shutdown")
    }

} // ce::application
