import os

from conan import ConanFile
from conan.tools.files import copy


class MultiplayerGameRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps"

    def requirements(self):
        self.requires("fmt/10.2.1")
        self.requires("spdlog/1.13.0")
        self.requires("glfw/3.3.8")

    def configure(self):
        pass

    def generate(self):
        pass
