#include "Engine/core/Memory.hpp"

#include "Engine/core/Logger.hpp"
#include "Engine/utils/ToString.hpp"
#include "Engine/platform/Platform.hpp"


namespace ce::memory {

    struct MemoryStats {
        uint64_t totalAllocated;
        uint64_t taggedAllocation[TagMax];
    };

    static MemoryStats stats;

    void setup() {
        platform::zeroMemory(&stats, sizeof(MemoryStats));

        LOG_INFO("Memory setup")
    }

    void shutdown() {
        LOG_INFO("Memory shutdown")
    }

    void* allocate(uint64_t size, Tag tag) {
        if (tag == TagUnknown) {
            LOG_WARN("Allocation called using {} tag. Re-class this allocation.", toString(TagUnknown))
        }

        stats.totalAllocated += size;
        stats.taggedAllocation[tag] += size;

        // TODO: Memory alignment
        void* block = platform::allocate(size, false);
        platform::zeroMemory(block, size);

        return block;
    }

    void free(void* block, uint64_t size, Tag tag) {
        if (tag == TagUnknown) {
            LOG_WARN("Free called using {} tag. Re-class this allocation.", toString(TagUnknown))
        }

        stats.totalAllocated -= size;
        stats.taggedAllocation[tag] -= size;

        // TODO: Memory alignment
        platform::free(block, false);
    }

    void* zeroMemory(void* block, uint64_t size) {
        return platform::zeroMemory(block, size);
    }

    void* copyMemory(void* dst, const void* src, uint64_t size) {
        return platform::copyMemory(dst, src, size);
    }

    void* setMemory(void* dst, int32_t value, uint64_t size) {
        return platform::setMemory(dst, value, size);
    }

    void getUsageLog() {
        const uint64_t kib = 1024;
        const uint64_t mib = (uint64_t)pow(kib, 2);
        const uint64_t gib = (uint64_t)pow(kib, 3);

        LOG_INFO("System memory use:")
        for (uint32_t i = 0; i < TagMax; ++i) {
            char unit[4] = "XiB";
            float amount = 1.0f;

            if (stats.taggedAllocation[i] >= gib) {
                unit[0] = 'G';
                amount = (float)stats.taggedAllocation[i] / (float)gib;
            } else if (stats.taggedAllocation[i] >= mib) {
                unit[0] = 'M';
                amount = (float)stats.taggedAllocation[i] / (float)mib;
            } else if (stats.taggedAllocation[i] >= kib) {
                unit[0] = 'K';
                amount = (float)stats.taggedAllocation[i] / (float)kib;
            } else {
                unit[0] = 'B';
                unit[1] = 0;
                amount = (float)stats.taggedAllocation[i];
            }

            LOG_INFO("\t- {}: {:.2f}{}", toString((Tag)i), amount, unit)
        }
    }

} // ce::memory
