#include "Engine/Engine.hpp"

#include "Engine/core/Logger.hpp"
#include "Engine/core/Memory.hpp"
#include "Engine/core/Application.hpp"


namespace ce {

    static Engine engine;
    static bool isInitialized = false;

    void run() {
        loop();
        shutdown();
    }

    void setup() {
        if (!ce::log::setup()) {
            return;
        }

        ce::memory::setup();

        // TODO: Make this configurable
        if (!ce::platform::setup(&engine.platform, "CombatGame", 1920, 1080)) {
            return;
        }

        if (!ce::application::setup(&engine)) {
            LOG_FATAL("Failed to setup application")
            return;
        }

        ce::memory::getUsageLog();

        LOG_INFO("Engine setup")

        isInitialized = true;
    }

    void loop() {
        if (!isInitialized) {
            return;
        }

        while (platform::processEvents(&engine.platform)) {
            update(0.0f);
            render();
        }
    }

    void update(float dt) {
        application::update(&engine, dt);
    }

    void render() {
        application::render(&engine);
    }

    void shutdown() {
        ce::application::shutdown(&engine);
        ce::platform::shutdown(&engine.platform);

        ce::memory::getUsageLog();

        LOG_INFO("Engine shutdown")

        ce::memory::shutdown();
        ce::log::shutdown();

        isInitialized = false;
    }

}
