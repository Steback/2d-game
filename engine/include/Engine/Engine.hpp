#ifndef COMBAT_ENGINE_ENGINE_HPP
#define COMBAT_ENGINE_ENGINE_HPP


#include "Engine/platform/Platform.hpp"


namespace ce {

    struct Engine {
        Platform platform;
    };

    void run();

    void setup();

    void loop();

    void update(float dt);

    void render();

    void shutdown();

} // ce


#endif //COMBAT_ENGINE_ENGINE_HPP
