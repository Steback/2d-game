#ifndef COMBAT_ENGINE_DEFINES_HPP
#define COMBAT_ENGINE_DEFINES_HPP


#define CE_ENGINE_NAME "Combat Engine"
#define CE_VERSION_MAJOR 1
#define CE_VERSION_MINOR 0
#define CE_VERSION_PATCH 0

// TODO Move this definitions to CMake side
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#define CE_PLATFORM_WINDOWS
#ifndef _WIN64
#error "64-bit is required on Windows!"
#endif
#elif defined(__linux__) || defined(__gnu_linux__)
// Linux OS
#define CE_PLATFORM_LINUX
#if defined(__ANDROID__)
#define CE_PLATFORM_ANDROID
#endif
#else
#error "Unknown platform!"
#endif

#ifndef __FILE_NAME__
#ifdef WIN32
#define __FILE_NAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#else
#define __FILE_NAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif
#endif


#endif //COMBAT_ENGINE_DEFINES_HPP
