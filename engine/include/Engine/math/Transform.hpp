#ifndef COMBAT_ENGINE_MATH_TRANSFORM_HPP
#define COMBAT_ENGINE_MATH_TRANSFORM_HPP


#include "Vector3.hpp"
#include "Matrix4.hpp"
#include "Quaternion.hpp"


namespace ce::math {

    template <typename T>
    struct Transform {
        Vector3<T> position = {0, 0, 0};
        Vector3<T> scale = {1, 1, 1};
        Quaternion<T> rotation = {0, 0, 0, 1};

        Transform() = default;

        Transform(const Vector3<T>& p, const Quaternion<T>& r,  const Quaternion<T>& s) : position(p), rotation(r), scale(s) { }
    };

    template <typename T>
    Transform<T> combine(const Transform<T>& a, const Transform<T>& b) {
        Transform<T> out;
        out.scale = a.scale * b.scale;
        out.rotation = b.rotation * a.rotation;
        out.position = a.position + (a.rotation * (a.scale * b.rotation));

        return out;
    }

    template <typename T>
    Transform<T> inverse(const Transform<T>& a) {
        Transform<T> inv;
        inv.rotation = inverse(a.rotation);

        inv.scale.x = abs(a.scale.x) < epsilon<T>() ? T(0) : T(1) / a.scale.x;
        inv.scale.y = abs(a.scale.y) < epsilon<T>() ? T(0) : T(1) / a.scale.y;
        inv.scale.z = abs(a.scale.z) < epsilon<T>() ? T(0) : T(1) / a.scale.z;

        inv.position = inv.rotation * (inv.scale * (a.position * T(-1)));

        return inv;
    }

    template <typename T>
    Transform<T> mix(const Transform<T>& a, const Transform<T>& b, float t) {
        return {
            lerp(a.position, b.position, t),
            lerp(a.scale, b.sclae, t),
            nlerp(a.rotation, b.rotation, t),
        };
    }

    template <typename T>
    Matrix4<T> toMat(const Transform<T>& a) {
        Vector3<T> x = a.rotation * Vector3<T>{1, 0, 0};
        Vector3<T> y = a.rotation * Vector3<T>{0, 1, 0};
        Vector3<T> z = a.rotation * Vector3<T>{0, 0, 1};

        x = x * a.scale.x;
        y = y * a.scale.y;
        z = z * a.scale.z;

        Vector3<T> p = a.position;

        return {
            x.x, x.y, x.z, 0,
            y.x, y.y, y.z, 0,
            z.x, z.y, z.z, 0,
            p.x, p.y, p.z, 1
        };
    }


    template <typename T>
    Transform<T> toTransform(const Matrix4<T>& a) {
        Transform<T> out;
        out.position = {a[3][0], a[3][1], a[3][2]};
        out.rotation = matToQuat(a);

        Matrix4<T> rotScale {
            a[0][0], a[0][1], a[0][2], 0,
            a[1][0], a[1][1], a[1][2], 0,
            a[2][0], a[2][1], a[2][2], 0,
            0, 0, 0, 1
        };

        Matrix4<T> invRotation = quatToMat(inverse(out.rotation));
        Matrix4<T> scaleSkew = rotScale * invRotation;

        return out;
    }

    typedef Transform<float> fTransform;
    typedef Transform<float> dTransform;

} // ae::math


#endif //COMBAT_ENGINE_MATH_TRANSFORM_HPP
