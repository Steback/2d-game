#ifndef COMBAT_ENGINE_MATH_MATH_DEFINES_HPP
#define COMBAT_ENGINE_MATH_MATH_DEFINES_HPP


#include <cmath>


namespace ce::math {

    inline int abs(int n) {
        return ::abs(n);
    }

    inline float abs(float n) {
        return ::fabsf(n);
    }

    inline double abs(double n) {
        return ::fabs(n);
    }

    inline float sqrt(float n) {
        return ::sqrtf(n);
    }

    inline double sqrt(double n) {
        return ::sqrt(n);
    }

    inline float sin(float a) {
        return ::sinf(a);
    }

    inline double sin(double a) {
        return ::sin(a);
    }

    inline float cos(float a) {
        return ::cosf(a);
    }

    inline double cos(double a) {
        return ::cos(a);
    }

    inline float tan(float a) {
        return ::tanf(a);
    }

    inline double tan(double a) {
        return ::tan(a);
    }

    inline float asin(float a) {
        return ::asinf(a);
    }

    inline double asin(double a) {
        return ::asin(a);
    }

    inline float acos(float a) {
        return ::acosf(a);
    }

    inline double acos(double a) {
        return ::acos(a);
    }

    inline float atan(float a) {
        return ::atanf(a);
    }

    inline double atan(double a) {
        return ::atan(a);
    }

    template <typename T>
    inline T epsilon() {
        return T(0.000001f);
    }

    template <typename T>
    inline T pi() {
        return T(3.14159265358979323846264338327950288);
    }

    template <typename T>
    inline T twoPi() {
        return T(6.28318530717958647692528676655900576);
    }

    template <typename T>
    inline T halfPi() {
        return T(1.57079632679489661923132169163975144);
    }

    template <typename T>
    inline T degrees(T radians) {
        // radians * (180 / PI)
        return radians * T(57.295779513082320876798154814105);
    }

    template <typename T>
    inline T radians(T degrees) {
        // radians * (PI / 180)
        return degrees * T(0.0174532925199432957692369076848);
    }

    template <typename T>
    inline void swap(T& a, T& b) {
        T t = a;
        a = b;
        b = t;
    }

    template <typename T>
    inline T clamp(T value, T min, T max) {
        return value <= min ? min : (value >= max ? max : value);
    }

} // ae::math


#endif //COMBAT_ENGINE_MATH_MATH_DEFINES_HPP
