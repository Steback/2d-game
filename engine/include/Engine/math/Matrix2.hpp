#ifndef ANIMATIONS_ENGINE_MATH_MATRIX2_HPP
#define ANIMATIONS_ENGINE_MATH_MATRIX2_HPP


#include "Math.hpp"
#include "Vector2.hpp"


namespace ce {

    namespace math {

        // Column-major
        template<typename T>
        struct Matrix2 {
            union {
                T values[4];
                T raw[2][2];
                Vector2 <T> columns[2];
            };

            explicit Matrix2(T n = T(1)) {
                raw[0][0] = n;
                raw[0][1] = 0;
                raw[1][1] = n;
                raw[1][0] = 0;
            }

            explicit Matrix2(const T *p) {
                values[0] = p[0];
                values[1] = p[1];
                values[2] = p[2];
                values[3] = p[3];
            }

            Matrix2(const Matrix2 &b) {
                values[0] = b.values[0];
                values[1] = b.values[1];
                values[2] = b.values[2];
                values[3] = b.values[3];
            }

            Matrix2(T _00, T _01, T _10, T _11) {
                raw[0][0] = _00;
                raw[0][1] = _01;

                raw[1][0] = _10;
                raw[1][1] = _11;
            }

            Matrix2(const Vector2 <T> &a, const Vector2 <T> &b) {
                columns[0] = a;
                columns[1] = b;
            }

            const Vector2 <T> &operator[](const unsigned int index) const {
                return columns[index];
            }

            Vector2 <T> &operator[](const unsigned int index) {
                return columns[index];
            }
        };

        template<typename T>
        Matrix2<T> operator+(const Matrix2<T> &a, const Matrix2<T> &b) {
            return {
                    a.raw[0][0] + b.raw[0][0], a.raw[0][1] + b.raw[0][1],
                    a.raw[1][0] + b.raw[1][0], a.raw[1][1] + b.raw[1][1],
            };
        }

        template<typename T>
        Matrix2<T> operator-(const Matrix2<T> &a, const Matrix2<T> &b) {
            return {
                    a.raw[0][0] - b.raw[0][0], a.raw[0][1] - b.raw[0][1],
                    a.raw[1][0] - b.raw[1][0], a.raw[1][1] - b.raw[1][1],
            };
        }

        template<typename T>
        Matrix2<T> operator*(const Matrix2<T> &a, T s) {
            return {
                    a.raw[0][0] * s, a.raw[0][1] * s,
                    a.raw[1][0] * s, a.raw[1][1] * s,
            };
        }

        template<typename T>
        Vector2 <T> operator*(const Matrix2<T> &a, const Vector2 <T> &b) {
            return {
                    a.raw[0][0] * b.x + a.raw[1][0] * b.y,
                    a.raw[0][1] * b.x + a.raw[1][1] * b.y,
            };
        }

        template<typename T>
        Matrix2<T> operator*(const Matrix2<T> &a, const Matrix2<T> &b) {
            const T _00 = a.raw[0][0] * b.raw[0][0] + a.raw[1][0] * b.raw[0][1];
            const T _01 = a.raw[0][0] * b.raw[1][0] + a.raw[1][0] * b.raw[1][1];

            const T _10 = a.raw[0][1] * b.raw[0][0] + a.raw[1][1] * b.raw[0][1];
            const T _11 = a.raw[0][1] * b.raw[1][0] + a.raw[1][1] * b.raw[1][1];

            // Column-major...
            return {
                    _00, _10,
                    _01, _11,
            };
        }

        // Not mathematically correct, but useful
        template<typename T>
        Matrix2<T> operator/(const Matrix2<T> &a, T s) {
            return {
                    a.raw[0][0] / s, a.raw[0][1] / s,
                    a.raw[1][0] / s, a.raw[1][1] / s,
            };
        }

        template<typename T>
        void transpose(Matrix2<T> &a) {
            swap(a.raw[0][1], a.raw[1][0]);
        }

        template<typename T>
        Matrix2<T> transposed(const Matrix2<T> &a) {
            return {
                    a.raw[0][0], a.raw[1][0],
                    a.raw[0][1], a.raw[1][1],
            };
        }

        template<typename T>
        T determinant(const Matrix2<T> &a) {
            return a.raw[0][0] * a.raw[1][1] - a.raw[1][0] * a.raw[0][1];
        }

        template<typename T>
        Matrix2<T> adjugate(const Matrix2<T> &y) {
            Matrix2<T> x;

            x.raw[0][0] = y.raw[1][1];
            x.raw[0][1] = -y.raw[0][1];

            x.raw[1][0] = -y.raw[1][0];
            x.raw[1][1] = y.raw[0][0];

            return transposed(x);
        }

        template<typename T>
        void invert(Matrix2<T> &m) {
            m = adjugate(m) / determinant(m);
        }

        template<typename T>
        Matrix2<T> inverse(const Matrix2<T> &m) {
            return adjugate(m) / determinant(m);
        }

        template<typename T>
        bool operator==(const Matrix2<T> &a, const Matrix2<T> &b) {
            return a.values[0] - b.values[0] < epsilon<T>() &&
                   a.values[1] - b.values[1] < epsilon<T>() &&
                   a.values[2] - b.values[2] < epsilon<T>() &&
                   a.values[3] - b.values[3] < epsilon<T>();
        }

        template<typename T>
        bool operator!=(const Matrix2<T> &a, const Matrix2<T> &b) {
            return !(a == b);
        }


    } // math

    typedef math::Matrix2<float> Mat2;
    typedef math::Matrix2<double> dMat2;
    typedef math::Matrix2<int> iMat2;
    typedef math::Matrix2<unsigned int> uMat2;

} // ce


#endif //ANIMATIONS_ENGINE_MATH_MATRIX2_HPP
