#ifndef COMBAT_ENGINE_MATH_VECTOR4_HPP
#define COMBAT_ENGINE_MATH_VECTOR4_HPP


#include "Math.hpp"


namespace ce {

    namespace math {

        template<typename T>
        struct Vector4 {
            union {
                struct {
                    T x;
                    T y;
                    T z;
                    T w;
                };
                T values[4];
            };

            explicit Vector4(T n = T(1)) {
                x = n;
                y = n;
                z = n;
                w = n;
            }

            explicit Vector4(const T *p) {
                values[0] = p[0];
                values[1] = p[1];
                values[2] = p[2];
                values[3] = p[3];
            }

            Vector4(const Vector4 &b) {
                values[0] = b.values[0];
                values[1] = b.values[1];
                values[2] = b.values[2];
                values[3] = b.values[3];
            }

            Vector4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

            const T &operator[](const unsigned int index) const {
                return values[index];
            }

            T &operator[](const unsigned int index) {
                return values[index];
            }
        };

        template<typename T>
        inline Vector4<T> operator+(const Vector4<T> &a, const Vector4<T> &b) {
            return {a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w};
        }

        template<typename T>
        inline Vector4<T> operator-(const Vector4<T> &a, const Vector4<T> &b) {
            return {a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w};
        }

        template<typename T>
        inline Vector4<T> operator*(const Vector4<T> &a, float s) {
            return {a.x * s, a.y * s, a.z * s, a.w * s};
        }

        template<typename T>
        inline Vector4<T> operator*(const Vector4<T> &a, const Vector4<T> &b) {
            return {a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w};
        }

        template<typename T>
        inline T dot(const Vector4<T> &a, const Vector4<T> &b) {
            return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
        }

        template<typename T>
        inline T length(const Vector4<T> &a) {
            return sqrt(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
        }

        template<typename T>
        inline T lengthSq(const Vector4<T> &a) {
            return a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w;
        }

        template<typename T>
        inline void normalize(Vector4<T> &a) {
            const T lenSq = sqrt(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
            a.x /= lenSq;
            a.y /= lenSq;
            a.z /= lenSq;
            a.w /= lenSq;
        }

        template<typename T>
        inline Vector4<T> normalized(const Vector4<T> &a) {
            const T lenSq = sqrt(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
            return {
                    a.x / lenSq,
                    a.y / lenSq,
                    a.z / lenSq,
                    a.w / lenSq
            };
        }

        template<typename T>
        inline float angle(const Vector4<T> &a, const Vector4<T> &b) {
            const T aLen = sqrt(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
            const T bLen = sqrt(b.x * b.x + b.y * b.y + b.z * b.z + b.w * b.w);
            const T dot = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
            return acos(dot / (aLen * bLen));
        }

        template<typename T>
        inline Vector4<T> project(const Vector4<T> &a, const Vector4<T> &b) {
            const T bLen = lengthSq(b);
            const T scale = dot(a, b) / bLen;
            return b * scale;
        }

        template<typename T>
        inline Vector4<T> reject(const Vector4<T> &a, const Vector4<T> &b) {
            return a - project(a, b);
        }

        template<typename T>
        inline Vector4<T> reflect(const Vector4<T> &a, const Vector4<T> &b) {
            const T bLen = length(b);
            const T scale = dot(a, b) / bLen;
            const Vector4<T> proj2 = b * (scale * 2);
            return a - proj2;
        }

        template<typename T>
        inline Vector4<T> lerp(const Vector4<T> &a, const Vector4<T> &b, T t) {
            return {
                    a.x + (b.x - a.x) * t,
                    a.y + (b.y - a.y) * t,
                    a.z + (b.z - a.z) * t,
                    a.w + (b.w - a.w) * t
            };
        }

        template<typename T>
        inline Vector4<T> slerp(const Vector4<T> &a, const Vector4<T> &b, T t) {
            const Vector4<T> from = normalized(a);
            const Vector4<T> to = normalized(b);
            const T theta = angle(from, to);
            const T sinTheta = sin(theta);
            const T ap = sin((T) 1 - t) * theta / sinTheta;
            const T bp = sin(t * theta) / sinTheta;

            return from * ap + to * bp;
        }

        template<typename T>
        inline Vector4<T> nlerp(const Vector4<T> &a, const Vector4<T> &b, T t) {
            return normalized({
                                      a.x + (b.x - a.x) * t,
                                      a.y + (b.y - a.y) * t,
                                      a.z + (b.z - a.z) * t,
                                      a.w + (b.w - a.w) * t
                              });
        }

        template<typename T>
        inline bool operator==(const Vector4<T> &a, const Vector4<T> &b) {
            const Vector4<T> diff = a - b;
            return lengthSq(diff) < epsilon<T>();
        }

        template<typename T>
        inline bool operator!=(const Vector4<T> &a, const Vector4<T> &b) {
            return !(a == b);
        }

    } // math

    typedef math::Vector4<int> iVec4;
    typedef math::Vector4<float> Vec4;
    typedef math::Vector4<double> dVec4;
    typedef math::Vector4<unsigned int> uiVec4;

} // ce


#endif //COMBAT_ENGINE_MATH_VECTOR4_HPP
