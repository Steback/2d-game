#ifndef COMBAT_ENGINE_MATH_VECTOR2_HPP
#define COMBAT_ENGINE_MATH_VECTOR2_HPP


#include "Math.hpp"


namespace ce {

    namespace math {

        template<typename T>
        struct Vector2 {
            union {
                struct {
                    T x;
                    T y;
                };
                T values[2];
            };

            explicit Vector2(T n = T(1)) {
                x = n;
                y = n;
            }

            explicit Vector2(const T *p) {
                values[0] = p[0];
                values[1] = p[1];
            }

            Vector2(const Vector2 &b) {
                values[0] = b.values[0];
                values[1] = b.values[1];
            }

            Vector2(T x, T y) : x(x), y(y) {}

            const T &operator[](const unsigned int index) const {
                return values[index];
            }

            T &operator[](const unsigned int index) {
                return values[index];
            }
        };

        template<typename T>
        inline Vector2<T> operator+(const Vector2<T> &a, const Vector2<T> &b) {
            return {a.x + b.x, a.y + b.y};
        }

        template<typename T>
        inline Vector2<T> operator-(const Vector2<T> &a, const Vector2<T> &b) {
            return {a.x - b.x, a.y - b.y};
        }

        template<typename T>
        inline Vector2<T> operator*(const Vector2<T> &a, float s) {
            return {a.x * s, a.y * s};
        }

        template<typename T>
        inline Vector2<T> operator*(const Vector2<T> &a, const Vector2<T> &b) {
            return {a.x * b.x, a.y * b.y};
        }

        template<typename T>
        inline T dot(const Vector2<T> &a, const Vector2<T> &b) {
            return a.x * b.x + a.y * b.y;
        }

        template<typename T>
        inline T length(const Vector2<T> &a) {
            return sqrt(a.x * a.x + a.y * a.y);
        }

        template<typename T>
        inline T lengthSq(const Vector2<T> &a) {
            return a.x * a.x + a.y * a.y;
        }

        template<typename T>
        inline void normalize(Vector2<T> &a) {
            const T lenSq = sqrt(a.x * a.x + a.y * a.y);
            a.x /= lenSq;
            a.y /= lenSq;
        }

        template<typename T>
        inline Vector2<T> normalized(const Vector2<T> &a) {
            const T lenSq = sqrt(a.x * a.x + a.y * a.y);
            return {
                    a.x / lenSq,
                    a.y / lenSq,
            };
        }

        template<typename T>
        inline float angle(const Vector2<T> &a, const Vector2<T> &b) {
            const T aLen = sqrt(a.x * a.x + a.y * a.y);
            const T bLen = sqrt(b.x * b.x + b.y * b.y);
            const T dot = a.x * b.x + a.y * b.y;
            return acos(dot / (aLen * bLen));
        }

        template<typename T>
        inline Vector2<T> project(const Vector2<T> &a, const Vector2<T> &b) {
            const T bLen = lengthSq(b);
            const T scale = dot(a, b) / bLen;
            return b * scale;
        }

        template<typename T>
        inline Vector2<T> reject(const Vector2<T> &a, const Vector2<T> &b) {
            return a - project(a, b);
        }

        template<typename T>
        inline Vector2<T> reflect(const Vector2<T> &a, const Vector2<T> &b) {
            const T bLen = length(b);
            const T scale = dot(a, b) / bLen;
            const Vector2<T> proj2 = b * (scale * 2);
            return a - proj2;
        }

        template<typename T>
        inline Vector2<T> lerp(const Vector2<T> &a, const Vector2<T> &b, T t) {
            return {
                    a.x + (b.x - a.x) * t,
                    a.y + (b.y - a.y) * t
            };
        }

        template<typename T>
        inline Vector2<T> slerp(const Vector2<T> &a, const Vector2<T> &b, T t) {
            const Vector2<T> from = normalized(a);
            const Vector2<T> to = normalized(b);
            const T theta = angle(from, to);
            const T sinTheta = sin(theta);
            const T ap = sin((T) 1 - t) * theta / sinTheta;
            const T bp = sin(t * theta) / sinTheta;

            return from * ap + to * bp;
        }

        template<typename T>
        inline Vector2<T> nlerp(const Vector2<T> &a, const Vector2<T> &b, T t) {
            return normalized({
                                      a.x + (b.x - a.x) * t,
                                      a.y + (b.y - a.y) * t
                              });
        }

        template<typename T>
        inline bool operator==(const Vector2<T> &a, const Vector2<T> &b) {
            const Vector2<T> diff = a - b;
            return lengthSq(diff) < epsilon<T>();
        }

        template<typename T>
        inline bool operator!=(const Vector2<T> &a, const Vector2<T> &b) {
            return !(a == b);
        }

    } // math

    typedef math::Vector2<int> iVec2;
    typedef math::Vector2<float> Vec2;
    typedef math::Vector2<double> dVec2;
    typedef math::Vector2<unsigned int> uiVec2;

} // ce

#endif //COMBAT_ENGINE_MATH_VECTOR2_HPP
