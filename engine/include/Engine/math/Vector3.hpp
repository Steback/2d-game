#ifndef COMBAT_ENGINE_MATH_VECTOR3_HPP
#define COMBAT_ENGINE_MATH_VECTOR3_HPP


#include "Math.hpp"


namespace ce {

    namespace math {

        template<typename T>
        struct Vector3 {
            union {
                struct {
                    T x;
                    T y;
                    T z;
                };
                T values[3];
            };

            explicit Vector3(T n = T(1)) {
                x = n;
                y = n;
                z = n;
            }

            explicit Vector3(const T *p) {
                values[0] = p[0];
                values[1] = p[1];
                values[2] = p[2];
            }

            Vector3(const Vector3 &b) {
                values[0] = b.values[0];
                values[1] = b.values[1];
                values[2] = b.values[2];
            }

            Vector3(T x, T y, T z) : x(x), y(y), z(z) {}

            const T &operator[](const unsigned int index) const {
                return values[index];
            }

            T &operator[](const unsigned int index) {
                return values[index];
            }
        };

        template<typename T>
        inline Vector3<T> operator+(const Vector3<T> &a, const Vector3<T> &b) {
            return {a.x + b.x, a.y + b.y, a.z + b.z};
        }

        template<typename T>
        inline Vector3<T> operator-(const Vector3<T> &a, const Vector3<T> &b) {
            return {a.x - b.x, a.y - b.y, a.z - b.z};
        }

        template<typename T>
        inline Vector3<T> operator*(const Vector3<T> &a, float s) {
            return {a.x * s, a.y * s, a.z * s};
        }

        template<typename T>
        inline Vector3<T> operator*(const Vector3<T> &a, const Vector3<T> &b) {
            return {a.x * b.x, a.y * b.y, a.z * b.z};
        }

        template<typename T>
        inline T dot(const Vector3<T> &a, const Vector3<T> &b) {
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        template<typename T>
        inline T length(const Vector3<T> &a) {
            return sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
        }

        template<typename T>
        inline T lengthSq(const Vector3<T> &a) {
            return a.x * a.x + a.y * a.y + a.z * a.z;
        }

        template<typename T>
        inline void normalize(Vector3<T> &a) {
            const T lenSq = sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
            a.x /= lenSq;
            a.y /= lenSq;
            a.z /= lenSq;
        }

        template<typename T>
        inline Vector3<T> normalized(const Vector3<T> &a) {
            const T lenSq = sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
            return {
                    a.x / lenSq,
                    a.y / lenSq,
                    a.z / lenSq,
            };
        }

        template<typename T>
        inline float angle(const Vector3<T> &a, const Vector3<T> &b) {
            const T aLen = sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
            const T bLen = sqrt(b.x * b.x + b.y * b.y + b.z * b.z);
            const T dot = a.x * b.x + a.y * b.y + a.z * b.z;
            return acos(dot / (aLen * bLen));
        }

        template<typename T>
        inline Vector3<T> project(const Vector3<T> &a, const Vector3<T> &b) {
            const T bLen = lengthSq(b);
            const T scale = dot(a, b) / bLen;
            return b * scale;
        }

        template<typename T>
        inline Vector3<T> reject(const Vector3<T> &a, const Vector3<T> &b) {
            return a - project(a, b);
        }

        template<typename T>
        inline Vector3<T> reflect(const Vector3<T> &a, const Vector3<T> &b) {
            const T bLen = length(b);
            const T scale = dot(a, b) / bLen;
            const Vector3<T> proj2 = b * (scale * 2);
            return a - proj2;
        }

        template<typename T>
        inline Vector3<T> cross(const Vector3<T> &a, const Vector3<T> &b) {
            return {
                    a.y * b.z - a.z * b.y,
                    a.z * b.x - a.x * b.z,
                    a.x * b.y - a.y * b.x
            };
        }

        template<typename T>
        inline Vector3<T> lerp(const Vector3<T> &a, const Vector3<T> &b, T t) {
            return {
                    a.x + (b.x - a.x) * t,
                    a.y + (b.y - a.y) * t,
                    a.z + (b.z - a.z) * t
            };
        }

        template<typename T>
        inline Vector3<T> slerp(const Vector3<T> &a, const Vector3<T> &b, T t) {
            const Vector3<T> from = normalized(a);
            const Vector3<T> to = normalized(b);
            const T theta = angle(from, to);
            const T sinTheta = sin(theta);
            const T ap = sin((T) 1 - t) * theta / sinTheta;
            const T bp = sin(t * theta) / sinTheta;

            return from * ap + to * bp;
        }

        template<typename T>
        inline Vector3<T> nlerp(const Vector3<T> &a, const Vector3<T> &b, T t) {
            return normalized({
                                      a.x + (b.x - a.x) * t,
                                      a.y + (b.y - a.y) * t,
                                      a.z + (b.z - a.z) * t
                              });
        }

        template<typename T>
        inline bool operator==(const Vector3<T> &a, const Vector3<T> &b) {
            const Vector3<T> diff = a - b;
            return lengthSq(diff) < epsilon<T>();
        }

        template<typename T>
        inline bool operator!=(const Vector3<T> &a, const Vector3<T> &b) {
            return !(a == b);
        }

    } // math

    typedef math::Vector3<int> iVec3;
    typedef math::Vector3<float> Vec3;
    typedef math::Vector3<double> dVec3;
    typedef math::Vector3<unsigned int> uiVec3;

} // ce


#endif //COMBAT_ENGINE_MATH_VECTOR3_HPP
