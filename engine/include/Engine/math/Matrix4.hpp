#ifndef COMBAT_ENGINE_MATH_MATRIX4_HPP
#define COMBAT_ENGINE_MATH_MATRIX4_HPP


#include "Math.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"


namespace ce {

    namespace math {

        // Column-major
        template<typename T>
        struct Matrix4 {
            union {
                T values[16];
                T raw[4][4];
                Vector4<T> columns[4];
                struct {
                    Vector4<T> right;
                    Vector4<T> up;
                    Vector4<T> forward;
                    Vector4<T> position;
                };
            };

            explicit Matrix4(T n = T(1)) {
                raw[0][0] = n;
                raw[0][1] = 0;
                raw[0][2] = 0;
                raw[0][3] = 0;

                raw[1][0] = 0;
                raw[1][1] = n;
                raw[1][2] = 0;
                raw[1][3] = 0;

                raw[2][0] = 0;
                raw[2][1] = 0;
                raw[2][2] = n;
                raw[2][3] = 0;

                raw[3][0] = 0;
                raw[3][1] = 0;
                raw[3][2] = 0;
                raw[3][3] = n;
            }

            explicit Matrix4(const T *p) {
                values[0] = p[0];
                values[1] = p[1];
                values[2] = p[2];
                values[3] = p[3];
                values[4] = p[4];
                values[5] = p[5];
                values[6] = p[6];
                values[7] = p[7];
                values[8] = p[8];
                values[9] = p[9];
                values[10] = p[10];
                values[11] = p[11];
                values[12] = p[12];
                values[13] = p[13];
                values[14] = p[14];
                values[15] = p[15];
            }

            Matrix4(const Matrix4 &b) {
                values[0] = b.values[0];
                values[1] = b.values[1];
                values[2] = b.values[2];
                values[3] = b.values[3];
                values[4] = b.values[4];
                values[5] = b.values[5];
                values[6] = b.values[6];
                values[7] = b.values[7];
                values[8] = b.values[8];
                values[9] = b.values[9];
                values[10] = b.values[10];
                values[11] = b.values[11];
                values[12] = b.values[12];
                values[13] = b.values[13];
                values[14] = b.values[14];
                values[15] = b.values[15];
            }

            Matrix4(T _00, T _01, T _02, T _03, T _10, T _11, T _12, T _13, T _20, T _21, T _22, T _23, T _30, T _31,
                    T _32, T _33) {
                raw[0][0] = _00;
                raw[0][1] = _01;
                raw[0][2] = _02;
                raw[0][3] = _03;

                raw[1][0] = _10;
                raw[1][1] = _11;
                raw[1][2] = _12;
                raw[1][3] = _13;

                raw[2][0] = _20;
                raw[2][1] = _21;
                raw[2][2] = _22;
                raw[2][3] = _23;

                raw[3][0] = _30;
                raw[3][1] = _31;
                raw[3][2] = _32;
                raw[3][3] = _33;
            }

            Matrix4(const Vector4<T> &a, const Vector4<T> &b, const Vector4<T> c, const Vector4<T> &d) {
                columns[0] = a;
                columns[1] = b;
                columns[2] = c;
                columns[3] = d;
            }

            const Vector4<T> &operator[](const unsigned int index) const {
                return columns[index];
            }

            Vector4<T> &operator[](const unsigned int index) {
                return columns[index];
            }
        };

        template<typename T>
        Matrix4<T> operator+(const Matrix4<T> &a, const Matrix4<T> &b) {
            return {
                    a.raw[0][0] + b.raw[0][0], a.raw[0][1] + b.raw[0][1], a.raw[0][2] + b.raw[0][2],
                    a.raw[0][3] + b.raw[0][3],
                    a.raw[1][0] + b.raw[1][0], a.raw[1][1] + b.raw[1][1], a.raw[1][2] + b.raw[1][2],
                    a.raw[1][3] + b.raw[1][3],
                    a.raw[2][0] + b.raw[2][0], a.raw[2][1] + b.raw[2][1], a.raw[2][2] + b.raw[2][2],
                    a.raw[2][3] + b.raw[2][3],
                    a.raw[3][0] + b.raw[3][0], a.raw[3][1] + b.raw[3][1], a.raw[3][2] + b.raw[3][2],
                    a.raw[3][3] + b.raw[3][3]
            };
        }

        template<typename T>
        Matrix4<T> operator-(const Matrix4<T> &a, const Matrix4<T> &b) {
            return {
                    a.raw[0][0] - b.raw[0][0], a.raw[0][1] - b.raw[0][1], a.raw[0][2] - b.raw[0][2],
                    a.raw[0][3] - b.raw[0][3],
                    a.raw[1][0] - b.raw[1][0], a.raw[1][1] - b.raw[1][1], a.raw[1][2] - b.raw[1][2],
                    a.raw[1][3] - b.raw[1][3],
                    a.raw[2][0] - b.raw[2][0], a.raw[2][1] - b.raw[2][1], a.raw[2][2] - b.raw[2][2],
                    a.raw[2][3] - b.raw[2][3],
                    a.raw[3][0] - b.raw[3][0], a.raw[3][1] - b.raw[3][1], a.raw[3][2] - b.raw[3][2],
                    a.raw[3][3] - b.raw[3][3]
            };
        }

        template<typename T>
        Matrix4<T> operator*(const Matrix4<T> &a, T s) {
            return {
                    a.raw[0][0] * s, a.raw[0][1] * s, a.raw[0][2] * s, a.raw[0][3] * s,
                    a.raw[1][0] * s, a.raw[1][1] * s, a.raw[1][2] * s, a.raw[1][3] * s,
                    a.raw[2][0] * s, a.raw[2][1] * s, a.raw[2][2] * s, a.raw[2][3] * s,
                    a.raw[3][0] * s, a.raw[3][1] * s, a.raw[3][2] * s, a.raw[3][3] * s
            };
        }

        template<typename T>
        Vector4<T> operator*(const Matrix4<T> &a, const Vector4<T> &b) {
            return {
                    a.raw[0][0] * b.x + a.raw[1][0] * b.y + a.raw[2][0] * b.z + a.raw[3][0] * b.w,
                    a.raw[0][1] * b.x + a.raw[1][1] * b.y + a.raw[2][1] * b.z + a.raw[3][1] * b.w,
                    a.raw[0][2] * b.x + a.raw[1][2] * b.y + a.raw[2][2] * b.z + a.raw[3][2] * b.w,
                    a.raw[0][3] * b.x + a.raw[1][3] * b.y + a.raw[2][3] * b.z + a.raw[3][3] * b.w
            };
        }

        template<typename T>
        Matrix4<T> operator*(const Matrix4<T> &a, const Matrix4<T> &b) {
            const T _00 = a.raw[0][0] * b.raw[0][0] + a.raw[1][0] * b.raw[0][1] + a.raw[2][0] * b.raw[0][2] +
                          a.raw[3][0] * b.raw[0][3];
            const T _01 = a.raw[0][0] * b.raw[1][0] + a.raw[1][0] * b.raw[1][1] + a.raw[2][0] * b.raw[1][2] +
                          a.raw[3][0] * b.raw[1][3];
            const T _02 = a.raw[0][0] * b.raw[2][0] + a.raw[1][0] * b.raw[2][1] + a.raw[2][0] * b.raw[2][2] +
                          a.raw[3][0] * b.raw[2][3];
            const T _03 = a.raw[0][0] * b.raw[3][0] + a.raw[1][0] * b.raw[3][1] + a.raw[2][0] * b.raw[3][2] +
                          a.raw[3][0] * b.raw[3][3];

            const T _10 = a.raw[0][1] * b.raw[0][0] + a.raw[1][1] * b.raw[0][1] + a.raw[2][1] * b.raw[0][2] +
                          a.raw[3][1] * b.raw[0][3];
            const T _11 = a.raw[0][1] * b.raw[1][0] + a.raw[1][1] * b.raw[1][1] + a.raw[2][1] * b.raw[1][2] +
                          a.raw[3][1] * b.raw[1][3];
            const T _12 = a.raw[0][1] * b.raw[2][0] + a.raw[1][1] * b.raw[2][1] + a.raw[2][1] * b.raw[2][2] +
                          a.raw[3][1] * b.raw[2][3];
            const T _13 = a.raw[0][1] * b.raw[3][0] + a.raw[1][1] * b.raw[3][1] + a.raw[2][1] * b.raw[3][2] +
                          a.raw[3][1] * b.raw[3][3];

            const T _20 = a.raw[0][2] * b.raw[0][0] + a.raw[1][2] * b.raw[0][1] + a.raw[2][2] * b.raw[0][2] +
                          a.raw[3][2] * b.raw[0][3];
            const T _21 = a.raw[0][2] * b.raw[1][0] + a.raw[1][2] * b.raw[1][1] + a.raw[2][2] * b.raw[1][2] +
                          a.raw[3][2] * b.raw[1][3];
            const T _22 = a.raw[0][2] * b.raw[2][0] + a.raw[1][2] * b.raw[2][1] + a.raw[2][2] * b.raw[2][2] +
                          a.raw[3][2] * b.raw[2][3];
            const T _23 = a.raw[0][2] * b.raw[3][0] + a.raw[1][2] * b.raw[3][1] + a.raw[2][2] * b.raw[3][2] +
                          a.raw[3][2] * b.raw[3][3];

            const T _30 = a.raw[0][3] * b.raw[0][0] + a.raw[1][3] * b.raw[0][1] + a.raw[2][3] * b.raw[0][2] +
                          a.raw[3][3] * b.raw[0][3];
            const T _31 = a.raw[0][3] * b.raw[1][0] + a.raw[1][3] * b.raw[1][1] + a.raw[2][3] * b.raw[1][2] +
                          a.raw[3][3] * b.raw[1][3];
            const T _32 = a.raw[0][3] * b.raw[2][0] + a.raw[1][3] * b.raw[2][1] + a.raw[2][3] * b.raw[2][2] +
                          a.raw[3][3] * b.raw[2][3];
            const T _33 = a.raw[0][3] * b.raw[3][0] + a.raw[1][3] * b.raw[3][1] + a.raw[2][3] * b.raw[3][2] +
                          a.raw[3][3] * b.raw[3][3];

            // Column-major...
            return {
                    _00, _10, _20, _30,
                    _01, _11, _21, _31,
                    _02, _12, _22, _32,
                    _03, _13, _23, _33
            };
        }

        // Not mathematically correct, but useful
        template<typename T>
        Matrix4<T> operator/(const Matrix4<T> &a, T s) {
            return {
                    a.raw[0][0] / s, a.raw[0][1] / s, a.raw[0][2] / s, a.raw[0][3] / s,
                    a.raw[1][0] / s, a.raw[1][1] / s, a.raw[1][2] / s, a.raw[1][3] / s,
                    a.raw[2][0] / s, a.raw[2][1] / s, a.raw[2][2] / s, a.raw[2][3] / s,
                    a.raw[3][0] / s, a.raw[3][1] / s, a.raw[3][2] / s, a.raw[3][3] / s
            };
        }

        template<typename T>
        void transpose(Matrix4<T> &a) {
            swap(a.raw[0][1], a.raw[1][0]);
            swap(a.raw[0][2], a.raw[2][0]);
            swap(a.raw[0][3], a.raw[3][0]);
            swap(a.raw[1][2], a.raw[2][1]);
            swap(a.raw[1][3], a.raw[3][1]);
            swap(a.raw[2][3], a.raw[3][2]);
        }

        template<typename T>
        Matrix4<T> transposed(const Matrix4<T> &a) {
            return {
                    a.raw[0][0], a.raw[1][0], a.raw[2][0], a.raw[3][0],
                    a.raw[0][1], a.raw[1][1], a.raw[2][1], a.raw[3][1],
                    a.raw[0][2], a.raw[1][2], a.raw[2][2], a.raw[3][2],
                    a.raw[0][3], a.raw[1][3], a.raw[2][3], a.raw[3][3],
            };
        }

        template<typename T>
        T determinant(const Matrix4<T> &a) {
            const T x = a.raw[0][0] * ((a.raw[1][1] * (a.raw[2][2] * a.raw[3][3] - a.raw[2][3] * a.raw[3][2])) -
                                       (a.raw[2][1] * (a.raw[1][2] * a.raw[3][3] - a.raw[1][3] * a.raw[3][2])) +
                                       (a.raw[3][1] * (a.raw[1][2] * a.raw[2][3] - a.raw[1][3] * a.raw[2][2])));

            const T y = a.raw[1][0] * ((a.raw[0][1] * (a.raw[2][2] * a.raw[3][3] - a.raw[2][3] * a.raw[3][2])) -
                                       (a.raw[2][1] * (a.raw[0][2] * a.raw[3][3] - a.raw[0][3] * a.raw[3][2])) +
                                       (a.raw[3][1] * (a.raw[0][2] * a.raw[2][3] - a.raw[0][3] * a.raw[2][2])));

            const T z = a.raw[2][0] * ((a.raw[0][1] * (a.raw[1][2] * a.raw[3][3] - a.raw[1][3] * a.raw[3][2])) -
                                       (a.raw[1][1] * (a.raw[0][2] * a.raw[3][3] - a.raw[0][3] * a.raw[3][2])) +
                                       (a.raw[3][1] * (a.raw[0][2] * a.raw[1][3] - a.raw[0][3] * a.raw[1][2])));

            const T w = a.raw[3][0] * ((a.raw[0][1] * (a.raw[1][2] * a.raw[2][3] - a.raw[1][3] * a.raw[2][2])) -
                                       (a.raw[1][1] * (a.raw[0][2] * a.raw[2][3] - a.raw[0][3] * a.raw[2][2])) +
                                       (a.raw[2][1] * (a.raw[0][2] * a.raw[1][3] - a.raw[0][3] * a.raw[1][2])));

            return x - y + z - w;
        }

        template<typename T>
        Matrix4<T> adjugate(const Matrix4<T> &y) {
            Matrix4<T> x;

            x.raw[0][0] = ((y.raw[1][1] * (y.raw[2][2] * y.raw[3][3] - y.raw[2][3] * y.raw[3][2])) -
                           (y.raw[2][1] * (y.raw[1][2] * y.raw[3][3] - y.raw[1][3] * y.raw[3][2])) +
                           (y.raw[3][1] * (y.raw[1][2] * y.raw[2][3] - y.raw[1][3] * y.raw[2][2])));

            x.raw[0][1] = ((y.raw[1][0] * (y.raw[2][2] * y.raw[3][3] - y.raw[2][3] * y.raw[3][2])) -
                           (y.raw[2][0] * (y.raw[1][2] * y.raw[3][3] - y.raw[1][3] * y.raw[3][2])) +
                           (y.raw[3][0] * (y.raw[1][2] * y.raw[2][3] - y.raw[1][3] * y.raw[2][2]))) * -1;

            x.raw[0][2] = ((y.raw[1][0] * (y.raw[2][1] * y.raw[3][3] - y.raw[2][3] * y.raw[3][1])) -
                           (y.raw[2][0] * (y.raw[1][1] * y.raw[3][3] - y.raw[1][3] * y.raw[3][1])) +
                           (y.raw[3][0] * (y.raw[1][1] * y.raw[2][3] - y.raw[1][3] * y.raw[2][1])));

            x.raw[0][3] = ((y.raw[1][0] * (y.raw[2][1] * y.raw[3][2] - y.raw[2][2] * y.raw[3][1])) -
                           (y.raw[2][0] * (y.raw[1][1] * y.raw[3][2] - y.raw[1][2] * y.raw[3][1])) +
                           (y.raw[3][0] * (y.raw[1][1] * y.raw[2][2] - y.raw[1][2] * y.raw[2][1]))) * -1;

            x.raw[1][0] = ((y.raw[0][1] * (y.raw[2][2] * y.raw[3][3] - y.raw[2][3] * y.raw[3][2])) -
                           (y.raw[2][1] * (y.raw[0][2] * y.raw[3][3] - y.raw[0][3] * y.raw[3][2])) +
                           (y.raw[3][1] * (y.raw[0][2] * y.raw[2][3] - y.raw[0][3] * y.raw[2][2]))) * -1;

            x.raw[1][1] = ((y.raw[0][0] * (y.raw[2][2] * y.raw[3][3] - y.raw[2][3] * y.raw[3][2])) -
                           (y.raw[2][0] * (y.raw[0][2] * y.raw[3][3] - y.raw[0][3] * y.raw[3][2])) +
                           (y.raw[3][0] * (y.raw[0][2] * y.raw[2][3] - y.raw[0][3] * y.raw[2][2])));

            x.raw[1][2] = ((y.raw[0][0] * (y.raw[2][1] * y.raw[3][3] - y.raw[2][3] * y.raw[3][1])) -
                           (y.raw[2][0] * (y.raw[0][1] * y.raw[3][3] - y.raw[0][3] * y.raw[3][1])) +
                           (y.raw[3][0] * (y.raw[0][1] * y.raw[2][3] - y.raw[0][3] * y.raw[2][1]))) * -1;

            x.raw[1][3] = ((y.raw[0][0] * (y.raw[2][1] * y.raw[3][2] - y.raw[2][2] * y.raw[3][1])) -
                           (y.raw[2][0] * (y.raw[0][1] * y.raw[3][2] - y.raw[0][2] * y.raw[3][1])) +
                           (y.raw[3][0] * (y.raw[0][1] * y.raw[2][2] - y.raw[0][2] * y.raw[2][1])));

            x.raw[2][0] = ((y.raw[0][1] * (y.raw[1][2] * y.raw[3][3] - y.raw[1][3] * y.raw[3][2])) -
                           (y.raw[1][1] * (y.raw[0][2] * y.raw[3][3] - y.raw[0][3] * y.raw[3][2])) +
                           (y.raw[3][1] * (y.raw[0][2] * y.raw[1][3] - y.raw[0][3] * y.raw[1][2])));

            x.raw[2][1] = ((y.raw[0][0] * (y.raw[1][2] * y.raw[3][3] - y.raw[1][3] * y.raw[3][2])) -
                           (y.raw[1][0] * (y.raw[0][2] * y.raw[3][3] - y.raw[0][3] * y.raw[3][2])) +
                           (y.raw[3][3] * (y.raw[0][2] * y.raw[1][3] - y.raw[0][3] * y.raw[1][2]))) * -1;

            x.raw[2][2] = ((y.raw[0][0] * (y.raw[1][1] * y.raw[3][3] - y.raw[1][3] * y.raw[3][1])) -
                           (y.raw[1][0] * (y.raw[0][1] * y.raw[3][3] - y.raw[0][3] * y.raw[3][1])) +
                           (y.raw[3][0] * (y.raw[0][1] * y.raw[1][3] - y.raw[0][3] * y.raw[1][1])));

            x.raw[2][3] = ((y.raw[0][0] * (y.raw[1][1] * y.raw[3][2] - y.raw[1][2] * y.raw[3][1])) -
                           (y.raw[1][0] * (y.raw[0][1] * y.raw[3][2] - y.raw[0][2] * y.raw[3][1])) +
                           (y.raw[3][0] * (y.raw[0][1] * y.raw[1][2] - y.raw[0][2] * y.raw[1][1]))) * -1;

            x.raw[3][0] = ((y.raw[0][1] * (y.raw[1][2] * y.raw[2][3] - y.raw[1][3] * y.raw[2][2])) -
                           (y.raw[1][1] * (y.raw[0][2] * y.raw[2][3] - y.raw[0][3] * y.raw[2][2])) +
                           (y.raw[2][1] * (y.raw[0][2] * y.raw[1][3] - y.raw[0][3] * y.raw[1][2]))) * -1;

            x.raw[3][1] = ((y.raw[0][0] * (y.raw[1][2] * y.raw[2][3] - y.raw[1][3] * y.raw[2][2])) -
                           (y.raw[1][0] * (y.raw[0][2] * y.raw[2][3] - y.raw[0][3] * y.raw[2][2])) +
                           (y.raw[2][0] * (y.raw[0][2] * y.raw[1][3] - y.raw[0][3] * y.raw[1][2])));

            x.raw[3][2] = ((y.raw[0][0] * (y.raw[1][1] * y.raw[2][3] - y.raw[1][3] * y.raw[2][1])) -
                           (y.raw[1][0] * (y.raw[0][1] * y.raw[2][3] - y.raw[0][3] * y.raw[2][1])) +
                           (y.raw[2][0] * (y.raw[0][1] * y.raw[1][3] - y.raw[0][3] * y.raw[1][1]))) * -1;

            x.raw[3][3] = ((y.raw[0][0] * (y.raw[1][1] * y.raw[2][2] - y.raw[1][2] * y.raw[2][1])) -
                           (y.raw[1][0] * (y.raw[0][1] * y.raw[2][2] - y.raw[0][2] * y.raw[2][1])) +
                           (y.raw[2][0] * (y.raw[0][1] * y.raw[1][2] - y.raw[0][2] * y.raw[1][1])));

            return transposed(x);
        }

        template<typename T>
        void invert(Matrix4<T> &m) {
            m = adjugate(m) / determinant(m);
        }

        template<typename T>
        Matrix4<T> inverse(const Matrix4<T> &m) {
            return adjugate(m) / determinant(m);
        }

        template<typename T>
        Matrix4<T> perspective(T fov, T aspect, T near, T far) {
            const T tanHalfFov = tan(fov / T(2));

            Matrix4<T> result(T(0));
            result[0][0] = T(1) / (aspect * tanHalfFov);
            result[1][1] = T(1) / (tanHalfFov);
            result[2][2] = (far + near) / (far - near);
            result[2][3] = T(1);
            result[3][2] = (T(2) * far * near) / (far - near);

            return result;
        }

        template<typename T>
        Matrix4<T> ortho(T left, T right, T bottom, T top, T near, T far) {
            Matrix4<T> result(T(1));
            result[0][0] = T(2) / (right - left);
            result[1][1] = T(2) / (top - bottom);
            result[2][2] = T(2) / (far - near);
            result[3][0] = -(right + left) / (right - left);
            result[3][1] = -(top + bottom) / (top - bottom);
            result[3][2] = -(far + near) / (far - near);

            return result;
        }

        template<typename T>
        Matrix4<T> lookAt(const Vector3 <T> &eye, const Vector3 <T> &center, const Vector3 <T> &up) {
            const Vector3 <T> f = normalize(center - eye);
            const Vector3 <T> s = normalize(cross(f, up));
            const Vector3 <T> u = cross(s, f);

            Matrix4<T> result;
            result[0][0] = s.x;
            result[1][0] = s.y;
            result[2][0] = s.z;
            result[0][1] = u.x;
            result[1][1] = u.y;
            result[2][1] = u.z;
            result[0][2] = -f.x;
            result[1][2] = -f.y;
            result[2][2] = -f.z;
            result[3][0] = -dot(s, eye);
            result[3][1] = -dot(u, eye);
            result[3][2] = dot(f, eye);

            return result;
        }

        template<typename T>
        bool operator==(const Matrix4<T> &a, const Matrix4<T> &b) {
            return a.values[0] - b.values[0] < epsilon<T>() &&
                   a.values[1] - b.values[1] < epsilon<T>() &&
                   a.values[2] - b.values[2] < epsilon<T>() &&
                   a.values[3] - b.values[3] < epsilon<T>() &&
                   a.values[4] - b.values[4] < epsilon<T>() &&
                   a.values[5] - b.values[5] < epsilon<T>() &&
                   a.values[6] - b.values[6] < epsilon<T>() &&
                   a.values[7] - b.values[7] < epsilon<T>() &&
                   a.values[8] - b.values[8] < epsilon<T>() &&
                   a.values[9] - b.values[9] < epsilon<T>() &&
                   a.values[10] - b.values[10] < epsilon<T>() &&
                   a.values[11] - b.values[11] < epsilon<T>() &&
                   a.values[12] - b.values[12] < epsilon<T>() &&
                   a.values[13] - b.values[13] < epsilon<T>() &&
                   a.values[14] - b.values[14] < epsilon<T>() &&
                   a.values[15] - b.values[15] < epsilon<T>();
        }

        template<typename T>
        bool operator!=(const Matrix4<T> &a, const Matrix4<T> &b) {
            return !(a == b);
        }

    } // math

    typedef math::Matrix4<float> Mat4;
    typedef math::Matrix4<double> dMat4;
    typedef math::Matrix4<int> iMat4;
    typedef math::Matrix4<unsigned int> uMat4;

} // ce


#endif //COMBAT_ENGINE_MATH_MATRIX4_HPP
