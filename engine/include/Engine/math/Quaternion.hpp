#ifndef COMBAT_ENGINE_MATH_QUATERNION_HPP
#define COMBAT_ENGINE_MATH_QUATERNION_HPP


#include "Math.hpp"
#include "Vector3.hpp"
#include "Matrix4.hpp"


namespace ce {

    namespace math {

        template<typename T>
        struct Quaternion {
            union {
                struct {
                    T x;
                    T y;
                    T z;
                    T w;
                };
                struct {
                    Vector3 <T> vector;
                    T scalar;
                };
                T values[4];
            };

            Quaternion() : x(0), y(0), z(0), w(1) {}

            Quaternion(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

            explicit Quaternion(const T *p) {
                values[0] = p[0];
                values[1] = p[1];
                values[2] = p[2];
                values[3] = p[3];
            }

            Quaternion(const Quaternion<T> &a) {
                values[0] = a.values[0];
                values[1] = a.values[1];
                values[2] = a.values[2];
                values[3] = a.values[3];
            }

            T &operator[](unsigned int index) { return values[index]; }

            const T &operator[](unsigned int index) const { return values[index]; }
        };

        template<typename T>
        Quaternion<T> operator+(const Quaternion<T> &a, const Quaternion<T> &b) {
            return {a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w};
        }

        template<typename T>
        Quaternion<T> operator-(const Quaternion<T> &a, const Quaternion<T> &b) {
            return {a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w};
        }

        template<typename T>
        Quaternion<T> operator*(const Quaternion<T> &a, T s) {
            return {a.x * s, a.y * s, a.z * s, a.w * s};
        }

        template<typename T>
        Vector3 <T> operator*(const Quaternion<T> &q, const Vector3 <T> &v) {
            return q.vector * T(2) * dot(q.vector, v) +
                   v * (q.scalar * q.scalar - dot(q.vector, q.vector)) +
                   cross(q.vector, v) * T(2) * q.scalar;
        }

        template<typename T>
        Quaternion<T> operator*(const Quaternion<T> &p, const Quaternion<T> &q) {
            return {
                    p.x * q.w + p.y * q.z - p.z * q.y + p.w * q.x,
                    -p.x * q.z + p.y * q.w + p.z * q.x + p.w * q.y,
                    p.x * q.y - p.y * q.x + p.z * q.w + p.w * q.z,
                    -p.x * q.x - p.y * q.y - p.z * q.z + p.w * q.w
            };
        }

        template<typename T>
        Quaternion<T> operator-(const Quaternion<T> &a) {
            return {-a.x, -a.y, -a.z, -a.w};
        }

        template<typename T>
        bool operator==(const Quaternion<T> &a, const Quaternion<T> &b) {
            return abs(a.x - b.x) <= epsilon<T>() &&
                   abs(a.y - b.y) <= epsilon<T>() &&
                   abs(a.z - b.z) <= epsilon<T>() &&
                   abs(a.w - b.w) <= epsilon<T>();
        }

        template<typename T>
        bool operator!=(const Quaternion<T> &a, const Quaternion<T> &b) {
            return !(a == b);
        }

        template<typename T>
        Quaternion<T> pow(const Quaternion<T> &q, T p) {
            const T angle = T(2) * acos(q.scalar);
            const Vector3 <T> axis = normalized(q.vector);

            const T halfCos = cos((p * angle) / T(2));
            const T halfSin = sin((p * angle) / T(2));

            return {
                    axis.x * halfSin,
                    axis.y * halfSin,
                    axis.z * halfSin,
                    halfCos
            };
        }

        template<typename T>
        Quaternion<T> angleAxis(T angle, const Vector3 <T> &axis) {
            Vector3 <T> norm = normalized(axis);
            T s = sin(angle / T(2));

            return {
                    norm.x * s,
                    norm.y * s,
                    norm.z * s,
                    cos(angle / T(2))
            };
        }

        template<typename T>
        Quaternion<T> fromTo(const Vector3 <T> &from, const Vector3 <T> &to) {
            const Vector3 <T> f = normalized(from);
            const Vector3 <T> t = normalized(to);

            if (f == t) {
                return {};
            }

            if (f == t * T(-1)) {
                Vector3 <T> ortho = Vector3<T>(1, 0, 0);
                if (abs(f.y) < abs(f.x)) {
                    ortho = Vector3 < T > {0, 1, 0};
                }

                if (abs(f.z) < abs(f.y) && abs(f.z) < abs(f.x)) {
                    ortho = Vector3 < T > {0, 0, 1};
                }

                const Vector3 <T> axis = normalized(cross(f, ortho));
                return {axis.x, axis.y, axis.z, 0};
            }

            const Vector3 <T> half = normalized(f + t);
            const Vector3 <T> axis = cross(f, half);

            return {axis.x, axis.y, axis.z, dot(f, half)};
        }

        template<typename T>
        Vector3 <T> axis(const Quaternion<T> &q) {
            return normalized(Vector3<T>(q.x, q.y, q.z));
        }

        template<typename T>
        T angle(const Quaternion<T> &q) {
            return T(2) * acos(q.w);
        }

        template<typename T>
        T dot(const Quaternion<T> &a, const Quaternion<T> &b) {
            return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
        }

        template<typename T>
        T length(const Quaternion<T> &a) {
            return sqrt(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
        }

        template<typename T>
        T lengthSq(const Quaternion<T> &a) {
            return a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w;
        }

        template<typename T>
        void normalize(Quaternion<T> &a) {
            const T len = sqrt(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
            a.x /= len;
            a.y /= len;
            a.z /= len;
            a.w /= len;
        }

        template<typename T>
        Quaternion<T> normalized(const Quaternion<T> &a) {
            const T len = sqrt(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
            return {
                    a.x / len,
                    a.y / len,
                    a.z / len,
                    a.w / len
            };
        }

        template<typename T>
        Quaternion<T> conjugate(const Quaternion<T> &q) {
            return {
                    -q.x,
                    -q.y,
                    -q.z,
                    q.w
            };
        }

        template<typename T>
        void invert(Quaternion<T> &q) {
            const T lenSq = q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w;
            q.x = -q.x / lenSq;
            q.y = -q.y / lenSq;
            q.z = -q.z / lenSq;
            q.w = q.w / lenSq;
        }

        template<typename T>
        Quaternion<T> inverse(const Quaternion<T> &q) {
            const T lenSq = q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w;
            return {
                    -q.x / lenSq,
                    -q.y / lenSq,
                    -q.z / lenSq,
                    q.w / lenSq
            };
        }

        template<typename T>
        Quaternion<T> mix(const Quaternion<T> &from, const Quaternion<T> &to, T t) {
            return from * (T(1) - t) + to * t;
        }

        template<typename T>
        Quaternion<T> nlerp(const Quaternion<T> &from, const Quaternion<T> &to, T t) {
            return normalize(from + (to - from) * t);
        }

        template<typename T>
        Quaternion<T> slerp(const Quaternion<T> &from, const Quaternion<T> &to, T t) {
            return normalize(((inversed(from) * to) ^ t) * from);
        }

        template<typename T>
        Matrix4<T> quatToMat(const Quaternion<T> &q) {
            Vector3 <T> r = q * Vector3<T>(1, 0, 0);
            Vector3 <T> u = q * Vector3<T>(0, 1, 0);
            Vector3 <T> f = q * Vector3<T>(0, 0, 1);

            return {
                    r.x, r.y, r.z, 0,
                    u.x, u.y, u.z, 0,
                    f.x, f.y, f.z, 0,
                    0, 0, 0, 1
            };
        }

        template<typename T>
        Quaternion<T> matToQuat(const Matrix4<T> &m) {
            T fourXSquaredMinus1 = m[0][0] - m[1][1] - m[2][2];
            T fourYSquaredMinus1 = m[1][1] - m[0][0] - m[2][2];
            T fourZSquaredMinus1 = m[2][2] - m[0][0] - m[1][1];
            T fourWSquaredMinus1 = m[0][0] + m[1][1] + m[2][2];

            int biggestIndex = 0;
            T fourBiggestSquaredMinus1 = fourWSquaredMinus1;
            if (fourXSquaredMinus1 > fourBiggestSquaredMinus1) {
                fourBiggestSquaredMinus1 = fourXSquaredMinus1;
                biggestIndex = 1;
            }

            if (fourYSquaredMinus1 > fourBiggestSquaredMinus1) {
                fourBiggestSquaredMinus1 = fourYSquaredMinus1;
                biggestIndex = 2;
            }

            if (fourZSquaredMinus1 > fourBiggestSquaredMinus1) {
                fourBiggestSquaredMinus1 = fourZSquaredMinus1;
                biggestIndex = 3;
            }

            T biggestVal = sqrt(fourBiggestSquaredMinus1 + T(1)) / T(2);
            T mult = static_cast<T>(0.25) / biggestVal;

            switch (biggestIndex) {
                case 0:
                    return Quaternion<T>((m[1][2] - m[2][1]) * mult, (m[2][0] - m[0][2]) * mult,
                                         (m[0][1] - m[1][0]) * mult, biggestVal);
                case 1:
                    return Quaternion<T>(biggestVal, (m[0][1] + m[1][0]) * mult, (m[2][0] + m[0][2]) * mult,
                                         (m[1][2] - m[2][1]) * mult);
                case 2:
                    return Quaternion<T>((m[0][1] + m[1][0]) * mult, biggestVal, (m[1][2] + m[2][1]) * mult,
                                         (m[2][0] - m[0][2]) * mult);
                case 3:
                    return Quaternion<T>((m[2][0] + m[0][2]) * mult, (m[1][2] + m[2][1]) * mult, biggestVal,
                                         (m[0][1] - m[1][0]) * mult);
                default: // Silence a -Wswitch-default warning in GCC. Should never actually get here. Assert is just for sanity.
                    return {};
            }
        }

    } // math

    typedef math::Quaternion<float> Quat;
    typedef math::Quaternion<double> dQuat;

} // ce


#endif //COMBAT_ENGINE_MATH_QUATERNION_HPP
