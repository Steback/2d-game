#ifndef COMBAT_ENGINE_MATH_MATRIX3_HPP
#define COMBAT_ENGINE_MATH_MATRIX3_HPP


#include "Math.hpp"
#include "Vector3.hpp"


namespace ce {

    namespace math {

        // Column-major
        template<typename T>
        struct Matrix3 {
            union {
                T values[9];
                T raw[3][3];
                Vector3<T> columns[3];
            };

            explicit Matrix3(T n = T(1)) {
                raw[0][0] = n;
                raw[0][1] = 0;
                raw[0][2] = 0;

                raw[1][0] = 0;
                raw[1][1] = n;
                raw[1][2] = 0;

                raw[2][0] = 0;
                raw[2][1] = 0;
                raw[2][2] = n;
            }

            explicit Matrix3(const T *p) {
                values[0] = p[0];
                values[1] = p[1];
                values[2] = p[2];
                values[3] = p[3];
                values[4] = p[4];
                values[5] = p[5];
                values[6] = p[6];
                values[7] = p[7];
                values[8] = p[8];
            }

            Matrix3(const Matrix3 &b) {
                values[0] = b.values[0];
                values[1] = b.values[1];
                values[2] = b.values[2];
                values[3] = b.values[3];
                values[4] = b.values[4];
                values[5] = b.values[5];
                values[6] = b.values[6];
                values[7] = b.values[7];
                values[8] = b.values[8];
            }

            Matrix3(T _00, T _01, T _02, T _10, T _11, T _12, T _20, T _21, T _22) {
                raw[0][0] = _00;
                raw[0][1] = _01;
                raw[0][2] = _02;

                raw[1][0] = _10;
                raw[1][1] = _11;
                raw[1][2] = _12;

                raw[2][0] = _20;
                raw[2][1] = _21;
                raw[2][2] = _22;
            }

            Matrix3(const Vector3<T> &a, const Vector3<T> &b, const Vector3<T> c) {
                columns[0] = a;
                columns[1] = b;
                columns[2] = c;
            }

            const Vector3<T> &operator[](const unsigned int index) const {
                return columns[index];
            }

            Vector3<T> &operator[](const unsigned int index) {
                return columns[index];
            }
        };

        template<typename T>
        Matrix3<T> operator+(const Matrix3<T> &a, const Matrix3<T> &b) {
            return {
                    a.raw[0][0] + b.raw[0][0], a.raw[0][1] + b.raw[0][1], a.raw[0][2] + b.raw[0][2],
                    a.raw[1][0] + b.raw[1][0], a.raw[1][1] + b.raw[1][1], a.raw[1][2] + b.raw[1][2],
                    a.raw[2][0] + b.raw[2][0], a.raw[2][1] + b.raw[2][1], a.raw[2][2] + b.raw[2][2],
            };
        }

        template<typename T>
        Matrix3<T> operator-(const Matrix3<T> &a, const Matrix3<T> &b) {
            return {
                    a.raw[0][0] - b.raw[0][0], a.raw[0][1] - b.raw[0][1], a.raw[0][2] - b.raw[0][2],
                    a.raw[1][0] - b.raw[1][0], a.raw[1][1] - b.raw[1][1], a.raw[1][2] - b.raw[1][2],
                    a.raw[2][0] - b.raw[2][0], a.raw[2][1] - b.raw[2][1], a.raw[2][2] - b.raw[2][2],
            };
        }

        template<typename T>
        Matrix3<T> operator*(const Matrix3<T> &a, T s) {
            return {
                    a.raw[0][0] * s, a.raw[0][1] * s, a.raw[0][2] * s,
                    a.raw[1][0] * s, a.raw[1][1] * s, a.raw[1][2] * s,
                    a.raw[2][0] * s, a.raw[2][1] * s, a.raw[2][2] * s,
            };
        }

        template<typename T>
        Vector3<T> operator*(const Matrix3<T> &a, const Vector3<T> &b) {
            return {
                    a.raw[0][0] * b.x + a.raw[1][0] * b.y + a.raw[2][0] * b.z,
                    a.raw[0][1] * b.x + a.raw[1][1] * b.y + a.raw[2][1] * b.z,
                    a.raw[0][2] * b.x + a.raw[1][2] * b.y + a.raw[2][2] * b.z,
            };
        }

        template<typename T>
        Matrix3<T> operator*(const Matrix3<T> &a, const Matrix3<T> &b) {
            const T _00 = a.raw[0][0] * b.raw[0][0] + a.raw[1][0] * b.raw[0][1] + a.raw[2][0] * b.raw[0][2];
            const T _01 = a.raw[0][0] * b.raw[1][0] + a.raw[1][0] * b.raw[1][1] + a.raw[2][0] * b.raw[1][2];
            const T _02 = a.raw[0][0] * b.raw[2][0] + a.raw[1][0] * b.raw[2][1] + a.raw[2][0] * b.raw[2][2];

            const T _10 = a.raw[0][1] * b.raw[0][0] + a.raw[1][1] * b.raw[0][1] + a.raw[2][1] * b.raw[0][2];
            const T _11 = a.raw[0][1] * b.raw[1][0] + a.raw[1][1] * b.raw[1][1] + a.raw[2][1] * b.raw[1][2];
            const T _12 = a.raw[0][1] * b.raw[2][0] + a.raw[1][1] * b.raw[2][1] + a.raw[2][1] * b.raw[2][2];

            const T _20 = a.raw[0][2] * b.raw[0][0] + a.raw[1][2] * b.raw[0][1] + a.raw[2][2] * b.raw[0][2];
            const T _21 = a.raw[0][2] * b.raw[1][0] + a.raw[1][2] * b.raw[1][1] + a.raw[2][2] * b.raw[1][2];
            const T _22 = a.raw[0][2] * b.raw[2][0] + a.raw[1][2] * b.raw[2][1] + a.raw[2][2] * b.raw[2][2];

            // Column-major...
            return {
                    _00, _10, _20,
                    _01, _11, _21,
                    _02, _12, _22,
            };
        }

        // Not mathematically correct, but useful
        template<typename T>
        Matrix3<T> operator/(const Matrix3<T> &a, T s) {
            return {
                    a.raw[0][0] / s, a.raw[0][1] / s, a.raw[0][2] / s,
                    a.raw[1][0] / s, a.raw[1][1] / s, a.raw[1][2] / s,
                    a.raw[2][0] / s, a.raw[2][1] / s, a.raw[2][2] / s,
            };
        }

        template<typename T>
        void transpose(Matrix3<T> &a) {
            swap(a.raw[0][1], a.raw[1][0]);
            swap(a.raw[0][2], a.raw[2][0]);
            swap(a.raw[1][2], a.raw[2][1]);
        }

        template<typename T>
        Matrix3<T> transposed(const Matrix3<T> &a) {
            return {
                    a.raw[0][0], a.raw[1][0], a.raw[2][0],
                    a.raw[0][1], a.raw[1][1], a.raw[2][1],
                    a.raw[0][2], a.raw[1][2], a.raw[2][2],
            };
        }

        template<typename T>
        T determinant(const Matrix3<T> &a) {
            const T x = a.raw[0][0] * (a.raw[1][1] * a.raw[2][2] - a.raw[1][2] * a.raw[2][1]);
            const T y = a.raw[1][0] * (a.raw[0][1] * a.raw[2][2] - a.raw[0][2] * a.raw[2][1]);
            const T z = a.raw[2][0] * (a.raw[0][1] * a.raw[1][2] - a.raw[0][2] * a.raw[1][1]);

            return x - y + z;
        }

        template<typename T>
        Matrix3<T> adjugate(const Matrix3<T> &y) {
            Matrix3<T> x;

            x.raw[0][0] = (y.raw[1][1] * y.raw[2][2] - y.raw[1][2] * y.raw[2][1]);
            x.raw[0][1] = (y.raw[1][0] * y.raw[2][2] - y.raw[1][2] * y.raw[2][0]) * T(-1);
            x.raw[0][2] = (y.raw[1][0] * y.raw[2][1] - y.raw[1][1] * y.raw[2][0]);

            x.raw[1][0] = (y.raw[0][1] * y.raw[2][2] - y.raw[0][2] * y.raw[2][1]) * T(-1);
            x.raw[1][1] = (y.raw[0][0] * y.raw[2][2] - y.raw[0][2] * y.raw[2][0]);
            x.raw[1][2] = (y.raw[0][0] * y.raw[2][1] - y.raw[0][1] * y.raw[2][0]) * T(-1);

            x.raw[2][0] = (y.raw[0][1] * y.raw[1][2] - y.raw[0][2] * y.raw[1][1]);
            x.raw[2][1] = (y.raw[0][0] * y.raw[1][2] - y.raw[0][2] * y.raw[1][0]) * T(-1);
            x.raw[2][2] = (y.raw[0][0] * y.raw[1][1] - y.raw[0][1] * y.raw[1][0]);

            return transposed(x);
        }

        template<typename T>
        void invert(Matrix3<T> &m) {
            m = adjugate(m) / determinant(m);
        }

        template<typename T>
        Matrix3<T> inverse(const Matrix3<T> &m) {
            return adjugate(m) / determinant(m);
        }

        template<typename T>
        bool operator==(const Matrix3<T> &a, const Matrix3<T> &b) {
            return a.values[0] - b.values[0] < epsilon<T>() &&
                   a.values[1] - b.values[1] < epsilon<T>() &&
                   a.values[2] - b.values[2] < epsilon<T>() &&
                   a.values[3] - b.values[3] < epsilon<T>() &&
                   a.values[4] - b.values[4] < epsilon<T>() &&
                   a.values[5] - b.values[5] < epsilon<T>() &&
                   a.values[6] - b.values[6] < epsilon<T>() &&
                   a.values[7] - b.values[7] < epsilon<T>() &&
                   a.values[8] - b.values[8] < epsilon<T>();
        }

        template<typename T>
        bool operator!=(const Matrix3<T> &a, const Matrix3<T> &b) {
            return !(a == b);
        }

    } // math

    typedef math::Matrix3<float> Mat3;
    typedef math::Matrix3<double> dMat3;
    typedef math::Matrix3<int> iMat3;
    typedef math::Matrix3<unsigned int> uMat3;

} // ce


#endif //COMBAT_ENGINE_MATH_MATRIX3_HPP
