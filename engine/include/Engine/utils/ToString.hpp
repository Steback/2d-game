#ifndef COMBAT_ENGINE_UTILS_TO_STRING_HPP
#define COMBAT_ENGINE_UTILS_TO_STRING_HPP


#include "Engine/core/Memory.hpp"


namespace ce {

    inline const char* toString(memory::Tag tag) {
        switch (tag) {
            case memory::TagUnknown:
                return "Unknown";
            case memory::TagMax:
                return "Max";
        }

        return "Unknown";
    }

} // ce


#endif //COMBAT_ENGINE_UTILS_TO_STRING_HPP
