#ifndef COMBAT_ENGINE_CORE_MEMORY_HPP
#define COMBAT_ENGINE_CORE_MEMORY_HPP


#include <cstdint>


namespace ce::memory {

    enum Tag {
        TagUnknown = 0,
        TagMax
    };

    void setup();

    void shutdown();

    void* allocate(uint64_t size, Tag tag);

    void free(void* block, uint64_t size, Tag tag);

    void* zeroMemory(void* block, uint64_t size);

    void* copyMemory(void* dst, const void* src, uint64_t size);

    void* setMemory(void* dst, int32_t value, uint64_t size);

    void getUsageLog();

}


#endif //COMBAT_ENGINE_CORE_MEMORY_HPP
