#ifndef COMBAT_ENGINE_CORE_APPLICATION_HPP
#define COMBAT_ENGINE_CORE_APPLICATION_HPP


#include <string>
#include <cstdint>

#include "Logger.hpp"


namespace ce {

    struct Engine;

    namespace application {

        bool setup(Engine* engine);

        void update(Engine* engine, float dt);

        void render(Engine* engine);

        void shutdown(Engine* engine);

    } // application

} // ce


#ifdef CE_DEFINE_MAIN
#include "Engine/Engine.hpp"


int main() {
    ce::setup();
    ce::run();
    return 0;
}
#endif


#endif //COMBAT_ENGINE_CORE_APPLICATION_HPP
