#ifndef COMBAT_ENGINE_CORE_LOGGER_HPP
#define COMBAT_ENGINE_CORE_LOGGER_HPP


#include <string>

#include <fmt/format.h>

#include "Engine/Defines.hpp"



#define LOG(level, ...) ce::log::log_fmt(level, "[{}:{}] {}", __FILE_NAME__, __LINE__, fmt::format(__VA_ARGS__))

#define LOG_INFO(...) LOG(ce::log::Info, __VA_ARGS__);
#define LOG_WARN(...) LOG(ce::log::Warn, __VA_ARGS__);
#define LOG_ERROR(...) LOG(ce::log::Error, __VA_ARGS__);
#define LOG_FATAL(...) LOG(ce::log::Fatal, __VA_ARGS__);

#if BUILD_DEBUG
#define LOG_DEBUG(...) LOG(ce::log::Debug, __VA_ARGS__);
#else
#define LOG_DEBUG(...)
#endif

#if BUILD_DEBUG
#define LOG_TRACE(...) LOG(ce::log::Trace, __VA_ARGS__);
#else
// Does nothing when LOG_TRACE_ENABLED != 1
#define LOG_TRACE(...)
#endif


namespace ce::log {

    enum Level {
        Fatal = 0,
        Error = 1,
        Warn = 2,
        Info = 3,
        Debug = 4,
        Trace = 5
    };

    bool setup(const std::string& filename = "runtime.log");

    void shutdown();

    void log(Level level, const char* message);

    template<typename... Args>
    void log_fmt(Level level, fmt::format_string<Args...> fmt, Args &&...args) {
        log(level, fmt::vformat(fmt, fmt::make_format_args(args...)).c_str());
    }

}  // ce::log


#endif //COMBAT_ENGINE_CORE_LOGGER_HPP
