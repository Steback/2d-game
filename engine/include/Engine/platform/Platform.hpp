#ifndef COMBAT_ENGINE_PLATFORM_PLATFORM_HPP
#define COMBAT_ENGINE_PLATFORM_PLATFORM_HPP


#include <cstdint>


namespace ce {

    struct Platform {
        void* internal{nullptr};
    };

    namespace platform {

        bool setup(Platform* platform, const char* name, int width, int height);

        void shutdown(Platform* platform);

        bool processEvents(Platform* platform);

        void* allocate(uint64_t size, bool aligned);

        void free(void* block, bool aligned);

        void* zeroMemory(void* block, uint64_t size);

        void* copyMemory(void* dst, const void* src, uint64_t size);

        void* setMemory(void* dst, int32_t value, uint64_t size);

        double getTime();

    } // platform

} // ce


#endif //COMBAT_ENGINE_PLATFORM_PLATFORM_HPP
