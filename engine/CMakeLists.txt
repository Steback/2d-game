### Target ###
set(TARGET_NAME Engine)

### Source Code ###
include_directories(include)
file(GLOB_RECURSE SOURCE_FILES source/*.cpp include/*.hpp)

### Dependencies ###
find_package(fmt REQUIRED)

### Target Definition ###
add_library(${TARGET_NAME} ${SOURCE_FILES})
target_link_libraries(${TARGET_NAME} fmt::fmt)
target_include_directories(${TARGET_NAME} PUBLIC include)

if (${DEBUG_ENABLE})
    set_target_properties(${TARGET_NAME} PROPERTIES OUTPUT_NAME "${TARGET_NAME}-Debug")
endif ()